package laba5;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializer {

	public static void serialization(MotorShow m, String file) {
		
		FileOutputStream out = null;
		ObjectOutputStream obj = null;
		try {
			out = new FileOutputStream("data\\" + file);	
			obj = new ObjectOutputStream(out);
			obj.writeObject(m);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				obj.close();
				out.close();	
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
		
		
	}
	
	public static MotorShow deserialization(String file) {
		
		FileInputStream in = null;
		ObjectInputStream obj = null;
		MotorShow m = null;
		try {
			in = new FileInputStream("data\\" + file);	
			obj = new ObjectInputStream(in);
			m = (MotorShow) obj.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				obj.close();
				in.close();	
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
		return m;
	}
	
}
