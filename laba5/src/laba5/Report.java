package laba5;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class Report {
	
	
	private static String createReport(MotorShow m) {
		
		StringBuilder report = new StringBuilder();
		
		report.append("Motor show - " + m.getName() + "\n\n");
		
		List<Car> cars = m.get();
		
		for (int i = 0; i < cars.size(); i++) {
			
			report.append("Car: " + cars.get(i).getCarBrand() + ", price " 
			+ cars.get(i).getPrice() + "\n");
			
			List<Client> clients = getClients(m.getMap(), cars.get(i));
			
			for (Client client : clients) {
				report.append("client name: " + client.getName() +  
						" | client phone number : " + client.getPhoneNumber() + "\n");
			}
			report.append("----------------------\navailable : ");
			if (cars.get(i).isAvailability()) {
				report.append("yes\n\n");
			} else {
				report.append("no\n\n");
			}
		}
		
		
		return report.toString();
	}
	
	private static List<Client> getClients(HashMap<Client, Car> map, Car car) {
		
		List<Client> clients = new ArrayList<>();
		Collection<Client> collect = map.keySet();
		
		for (Client key : collect) {
			Car car1 = map.get(key);
			if (key != null) {
				if (car.equals(car1)) {
					clients.add(key);
				}
			}
		}
		return clients;
	}
	
	public static void print(MotorShow m) {
		System.out.println(createReport(m));
	}
	
	public static void createAndWriteReportToFile(MotorShow m, String file)  { //����� ������ ������ � ����
		String report = createReport(m);
		FileOutputStream repOut = null;
		try {
			repOut = new FileOutputStream("reports\\" + file);
			repOut.write(report.getBytes());	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				repOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
     }
	
	
	public static String openReport(String file) { //������ �� ������
		FileInputStream repIn = null;
		try {
			repIn = new FileInputStream("reports\\" + file);
			int a = repIn.available();
			byte[] b = new byte[a];
			repIn.read(b);
			return new String(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				repIn.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		return null;
	}
}
