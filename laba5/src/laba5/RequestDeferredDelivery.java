package laba5;

class RequestDeferredDelivery extends RequestToBuy{
	
	private int sale = 0;
	
	public RequestDeferredDelivery(Client client, int sale) {
		super(client);
		this.sale = sale;
	}

	
	public double calculateCost() {

		return price - (price / 100) * sale;
	}

	public int getSale() {
		return sale;
	}

	public void setSale(int sale) {
		this.sale = sale;
	}

}
