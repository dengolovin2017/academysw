package laba7;

import java.io.Serializable;

public class Client  implements Serializable {
	
	private String name;
	private int phoneNumber;
	private String typeR;
	private int sale;
	private double price;
	
	public Client() {
		name = "";
		phoneNumber = 0;
		typeR = "";
		sale = 0;
	}
	
	public Client(String name, int phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getTypeR() {
		return typeR;
	}
	public void setTypeR(String typeR) {
		this.typeR = typeR;
	}
	
	public int getSale() {
		return sale;
	}
	public void setSale(int sale) {
		this.sale = sale;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String toString() {
		return name + " - " + Integer.toString(phoneNumber);
	}
}
