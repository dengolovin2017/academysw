package laba7;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.beans.*;

public class XMLCoder {
	
	public static void encoder(MotorShow m, String file) {
		XMLEncoder encoder = null;
		try {
			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("data\\" + file)));
			encoder.writeObject(m);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (encoder != null) {
				encoder.close();
			}
		}
	}
	
	public static MotorShow decoder(String file) {
		XMLDecoder decoder = null;
		MotorShow m = null;
		try {
			decoder = new XMLDecoder(new FileInputStream("data\\" + file));
			m = (MotorShow) decoder.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (decoder != null) {
				decoder.close();
			}
		}
		return m;
	}

}
