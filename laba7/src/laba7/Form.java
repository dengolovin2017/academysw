package laba7;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JScrollBar;

public class Form {

	private JFrame frame;
	private DefaultListModel<Car> DLMCar;
	private DefaultListModel<Client> DLMClient;
	private String title;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	
	public JFrame getFrame() {
		return frame;
	}
	

	public Form(MotorShow m) {
		DLMCar = new DefaultListModel<>();
		DLMClient = new DefaultListModel<>();
		for (Car car : m.getCars()) {
			DLMCar.addElement(car);
		}
		List<Client> clients = new ArrayList<>(m.getMap().keySet());
		for (Client client : clients) {
			DLMClient.addElement(client);
		}
		title = m.getName();
		initialize();
		
	}

	private void initialize() {
		frame = new JFrame(title);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("���������� �� ����������", null, panel_1, null);
		panel_1.setLayout(null);
		
		JList<Car> list = new JList();
		list.setBounds(10, 13, 145, 209);
		panel_1.add(list);
		list.setModel(DLMCar);
		
		JLabel lblNewLabel = new JLabel("\u041C\u0430\u0440\u043A\u0430");
		lblNewLabel.setBounds(165, 14, 62, 14);
		panel_1.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("\u041A\u043E\u043B-\u0432\u043E \u043F\u0430\u0441\u0441\u0430\u0436\u0438\u0440\u043E\u0432");
		lblNewLabel_1.setBounds(165, 39, 119, 14);
		panel_1.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("\u0426\u0435\u043D\u0430");
		lblNewLabel_2.setBounds(165, 68, 62, 14);
		panel_1.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("\u041A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u043E \u043D\u0430 \u0441\u043A\u043B\u0430\u0434\u0435");
		lblNewLabel_3.setBounds(165, 93, 134, 14);
		panel_1.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("\u0412 \u043D\u0430\u043B\u0438\u0447\u0438\u0438");
		lblNewLabel_4.setBounds(165, 118, 74, 14);
		panel_1.add(lblNewLabel_4);
		
		textField = new JTextField();
		textField.setBounds(296, 11, 123, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(296, 36, 123, 20);
		panel_1.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(296, 65, 123, 20);
		panel_1.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(296, 90, 123, 20);
		panel_1.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(296, 115, 123, 20);
		panel_1.add(textField_4);
		
		JButton btnNewButton = new JButton("\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { //******************
				Car car = list.getSelectedValue();
				textField.setText(car.getCarBrand());
				textField_1.setText(Integer.toString(car.getMaxNumberPass()));
				textField_2.setText(Double.toString(car.getPrice()));
				textField_3.setText(Integer.toString(car.getStockAmount()));
				if (car.getStockAmount() > 0) {
					textField_4.setText("����");
				} else {
					textField_4.setText("���");
				}
			}
		});
		btnNewButton.setBounds(244, 176, 89, 23);
		panel_1.add(btnNewButton);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("������", null, panel, null);
		panel.setLayout(null);
		
		JList<Client> list_1 = new JList();
		list_1.setBounds(10, 11, 144, 211);
		panel.add(list_1);
		list_1.setModel(DLMClient);
		
		JButton btnNewButton_1 = new JButton("\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044C");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client client = list_1.getSelectedValue();
				textField_5.setText(client.getTypeR());
				textField_6.setText(Integer.toString(client.getSale()));
			}
		});
		btnNewButton_1.setBounds(237, 177, 98, 23);
		panel.add(btnNewButton_1);
		
		JLabel lblNewLabel_5 = new JLabel("\u0422\u0438\u043F \u0437\u0430\u044F\u0432\u043A\u0438");
		lblNewLabel_5.setBounds(164, 12, 80, 14);
		panel.add(lblNewLabel_5);
		
		JLabel lblNewLabel_5_1 = new JLabel("\u041F\u0440\u043E\u0446\u0435\u043D\u0442 \u0441\u043A\u0438\u0434\u043A\u0438");
		lblNewLabel_5_1.setBounds(164, 43, 98, 14);
		panel.add(lblNewLabel_5_1);
		
		textField_5 = new JTextField();
		textField_5.setBounds(264, 9, 144, 20);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(264, 40, 144, 20);
		panel.add(textField_6);
		
		JButton btnNewButton_2 = new JButton("\u0420\u0430\u0441\u0447\u0435\u0442 \u0441\u0442\u043E\u0438\u043C\u043E\u0441\u0442\u0438 \u0437\u0430\u043A\u0430\u0437\u0430");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//JOptionPane pane = new JOptionPane();
				JOptionPane.showMessageDialog(btnNewButton_2, "��������� ������!\n\n" 
				+ list_1.getSelectedValue().getPrice() + " ���");
			}
		});
		btnNewButton_2.setBounds(192, 143, 189, 23);
		panel.add(btnNewButton_2);
		
	}
}
