package laba7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MotorShow implements Serializable {
	
	private String name;
	private List<Car> cars;
	private static Scanner in;
	private HashMap<Client, Car> map;
	
	public MotorShow() {
		name = "";
		cars = new ArrayList<>();
		map = new HashMap<>();
	}
	
	public MotorShow(String name) {
		this.name = name;
		cars = new ArrayList<>();
		map = new HashMap<>();
	}

	
	public HashMap<Client, Car> getMap() {
		return map;
	}
	
	public void setMap(HashMap<Client, Car> map) {
		this.map = map;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void add(Car car) {
		cars.add(car);
	}
	
	public void remove(Car car) {
		cars.remove(car);
	}
	
	public List<Car> getCars() {
		return cars;
	}	
	
	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
	public String createRequestStand(RequestToBuy req) { 

		RequestFromStand r = new RequestFromStand(req.getClient());
		
		
		if (cars.size() == 0) {
			try {
				throw new KolichestvoAvtomobileyException("����������� ���!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		}
		
		
		double price = -1;
		int number = -1;
		
		
		System.out.println("\n(" + req.getClient().getName() + ") �������� ����� ����\n");
		for (int i = 0; i < cars.size(); i++) {
			System.out.println((i + 1) + ") " + cars.get(i));
		}
		for(;;) {	
			in = new Scanner(System.in);
			try {
				number = in.nextInt();
				if (cars.get(number - 1).isAvailability()) {
					cars.get(number - 1).decrementStockAmount();
					price = cars.get(number - 1).getPrice();
				}
				break;
			} catch (InputMismatchException e) {
				System.err.println("�� ����� �� �����!");
				e.printStackTrace();
			} catch (Exception e) {
				System.err.println("�� ����� �������� �����!");
				e.printStackTrace();	
			}
		}
			
		r.setPrice(price);
		r.calculateCost();
		if (price <= 0) {
			try {
				throw new KolichestvoAvtomobileyException("���������� ������ ����� �����������!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		} else {
			map.put(req.getClient(), cars.get(number - 1));
			return "�� ������ " + cars.get(number - 1).getCarBrand() + " �� " + price + " ���";
		}
	}
	
	public String createRequestDelivery(RequestToBuy req, int sale) { 

		RequestDeferredDelivery r = new RequestDeferredDelivery(req.getClient(), sale);
		
		if (cars.size() == 0) {
			try {
				throw new KolichestvoAvtomobileyException("����������� ���!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		}
		
		double price = -1;
		int number;
		
		System.out.println("\n(" + req.getClient().getName() + ")�������� ����� ����\n");
		for (int i = 0; i < cars.size(); i++) {
			System.out.println((i + 1) + ") " + cars.get(i));
		}
		
		for(;;) {	
			in = new Scanner(System.in);
			try {
				number = in.nextInt();
				if (cars.get(number - 1).isAvailability()) {
					cars.get(number - 1).decrementStockAmount();
					price = cars.get(number - 1).getPrice();
				}
				break;
			} catch (InputMismatchException e) {
				System.err.println("�� ����� �� �����!");
				e.printStackTrace();
			} catch (Exception e) {
				System.err.println("�� ����� �������� �����!");
				e.printStackTrace();	
			}
		}
		
		r.setPrice(price);
		price = r.calculateCost();
		if (price <= 0) {
			try {
				throw new KolichestvoAvtomobileyException("���������� ������ ����� �����������!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		} else {
			map.put(req.getClient(), cars.get(number - 1));
			return "�� ������ " + cars.get(number - 1).getCarBrand() + " �� " + price + " ��� �� ������� " + sale + "%";
		}
	}
	
	
	public String createRequestStand(RequestToBuy req, int num) { 

		RequestFromStand r = new RequestFromStand(req.getClient());
		
		
		if (cars.size() == 0) {
			try {
				throw new KolichestvoAvtomobileyException("����������� ���!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		}
		
		
		double price = cars.get(num - 1).getPrice();
		Client client = req.getClient();
		client.setTypeR("�� ������");
		client.setPrice(price);
			
		r.setPrice(price);
		r.calculateCost();
		if (price <= 0) {
			try {
				throw new KolichestvoAvtomobileyException("���������� ������ ����� �����������!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		} else {
			map.put(req.getClient(), cars.get(num - 1));
			return "�� ������ " + cars.get(num - 1).getCarBrand() + " �� " + price + " ���";
		}
	}
	
	public String createRequestDelivery(RequestToBuy req, int sale, int num) { 

		RequestDeferredDelivery r = new RequestDeferredDelivery(req.getClient(), sale);
		
		if (cars.size() == 0) {
			try {
				throw new KolichestvoAvtomobileyException("����������� ���!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		}
		
		double price = cars.get(num - 1).getPrice();
		Client client = req.getClient();
		client.setSale(sale);
		client.setTypeR("���������� ��������");
				
		r.setPrice(price);
		price = r.calculateCost();
		
		client.setPrice(price);
		
		if (price <= 0) {
			try {
				throw new KolichestvoAvtomobileyException("���������� ������ ����� �����������!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		} else {
			map.put(req.getClient(), cars.get(num - 1));
			return "�� ������ " + cars.get(num - 1).getCarBrand() + " �� " + price + " ��� �� ������� " + sale + "%";
		}
	}
	
	
	
	
	
}
