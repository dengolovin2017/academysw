package laba7;

class RequestFromStand extends RequestToBuy {
	
	public RequestFromStand(Client client) {
		super(client);
	}

	public double calculateCost() {
		return price;
	}
}
