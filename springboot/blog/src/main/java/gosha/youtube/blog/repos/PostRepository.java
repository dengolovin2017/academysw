package gosha.youtube.blog.repos;

import gosha.youtube.blog.models.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Long> {
}
