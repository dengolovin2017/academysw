import java.util.List;
import java.util.concurrent.RecursiveAction;

public class Task extends RecursiveAction {

    private List<String> listLinks;
    private static final int THREADS_TOTAL = 30;

    public Task(List<String> listLinks) {
        this.listLinks = listLinks;
    }

    @Override
    protected void compute() {
        if (listLinks.size() <= THREADS_TOTAL) {
            for (int i = 0; i < listLinks.size(); i++) {
                if (!MainClass.getCheckedLinks().contains(listLinks.get(i))) {
                    RecursiveActionClass action = new RecursiveActionClass(listLinks.get(i));
                }
            }
        } else {
            int middle = listLinks.size() / 2;
            List<String> newList = listLinks.subList(middle, listLinks.size());
            listLinks = listLinks.subList(0, middle);
            Task task = new Task(newList);
            task.fork();
            this.compute();
        }
    }
}
