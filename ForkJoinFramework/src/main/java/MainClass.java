import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MainClass {

    private static List<String> checkedLinks = new ArrayList<>();
    private static List<String> checked = new ArrayList<>();
    private static String tab;
    private static StringBuilder result = new StringBuilder();
    private static String mainLink = "https://lenta.ru";
    private static String nameFile = "lenta";

    public static void main(String[] args) {

        RecursiveActionClass action = new RecursiveActionClass(mainLink);

        checkedLinks = sortedList(checkedLinks);
        recursiveSorting(mainLink, -1);
        writeFiles(result.toString(), nameFile);

    }

    private static void writeFiles(String data, String name) {
        String path = "maps/" + name + ".txt";
        try {
            Files.write(Paths.get(path), data.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void recursiveSorting(String link, int num) {

        if (checked.contains(link)) {
            return;
        }
        num++;
        for (int i = 0; i < num; i++) {
            tab += "\t";
        }
        result.append(tab + link + "\n");
        checked.add(link);

        for (int i = 0; i < checkedLinks.size(); i++) {
            if (checkedLinks.get(i).equals(link)) {
                continue;
            }
            if (checkedLinks.get(i).contains(link) &&
                    ((link.split("\\/").length + 1) == checkedLinks.get(i).split("\\/").length)) {
                tab = "\t";
                recursiveSorting(checkedLinks.get(i), num);
            }
            if (checkedLinks.get(i).contains(link) &&
                    ((link.split("\\/").length + 2) == checkedLinks.get(i).split("\\/").length)) {
                tab = "\t";
                recursiveSorting(checkedLinks.get(i), num);
            }
        }
    }

    public static List<String> getCheckedLinks() {
        return checkedLinks;
    }

    public static void updateList(String link) {
        checkedLinks.add(link);
    }

    public static String getLink() {
        return mainLink;
    }

    private static List<String> sortedList(List<String> list) {
        return list.stream()
                .sorted(Comparator.comparing(c -> c.split("\\/").length))
                .collect(Collectors.toList());
    }
}