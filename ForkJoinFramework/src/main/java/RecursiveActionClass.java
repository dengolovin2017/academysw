import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;


public class RecursiveActionClass {

    private static String link;
    private Document doc;
    private Elements elements;
    private List<String> listLink = new ArrayList<>();


    public RecursiveActionClass(String link) {

        this.link = link;
        parsing(link);
    }

    private void parsing(String link) {

        if (link.charAt(link.length() - 1) == '/') {
            link = link.substring(0, link.length() - 1);
        }
        try {
            doc = Jsoup.connect(link).get(); //подключение
            enumeration(); //парсинг

        } catch (IOException e) {
            System.err.print("!");
            //e.printStackTrace();
        }

        ForkJoinPool fjp = new ForkJoinPool(3);
        fjp.invoke(new Task(listLink));
        listLink.clear();
    }

    private void enumeration() {
        if (!MainClass.getCheckedLinks().contains(link)) {

            System.out.print(".");
            MainClass.updateList(link);
            try {
                doc = Jsoup.connect(link).ignoreContentType(false).get();
            } catch (HttpStatusException e) {
                System.err.print("!");
                //e.printStackTrace();
            } catch (SocketTimeoutException e) {
                System.err.print("!");
                //e.printStackTrace();
            } catch (Exception e) {
                System.err.print("!");
                //e.printStackTrace();
            }
            elements = doc.select("a");

            for (Element e : elements) {
                if (e.attr("href").contains(".pdf")
                        || e.attr("href").contains(".docx")
                        || e.attr("href").contains(".rar")
                        || e.attr("href").contains(".doc")
                        || e.attr("href").contains(".zip")
                        || e.attr("href").contains(".xls")
                        || e.attr("href").contains(".rtf")
                ) {
                    System.err.print("!");
                } else {
                    check(e, link);
                }
            }
        }
    }

    private void check(Element e, String mainLink) {
        if (e.attr("href").matches("^\\/.+")  //"^\\/.+\\/$"
                && !listLink.contains(mainLink + e.attr("href"))) { // + "/"
            listLink.add(MainClass.getLink() + e.attr("href")); // + "/");
        }
    }
}