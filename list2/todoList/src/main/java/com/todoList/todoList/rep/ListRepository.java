package com.todoList.todoList.rep;

import com.todoList.todoList.model.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface ListRepository extends CrudRepository<List, Long> {
}
