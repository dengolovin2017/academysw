package com.todoList.todoList.controller;

import com.todoList.todoList.exceptions.*;
import com.todoList.todoList.model.List;
import com.todoList.todoList.model.Task;
import com.todoList.todoList.rep.ListRepository;
import com.todoList.todoList.rep.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
public class TaskController {

    @Autowired
    private ListRepository todoRep;
    @Autowired
    private TaskRepository taskRep;




    @GetMapping("/lists/{id}/{taskId}") //получение одного таска
    public ResponseEntity<?> getList(@PathVariable("id") Long id, @PathVariable("taskId") Long taskId) {
        Optional<List> list = todoRep.findById(id);
        Optional<Task> task = taskRep.findById(taskId);

        if (list.isPresent()) {
            if (task.isPresent()) {
                return new ResponseEntity<>(taskRep.findById(taskId), HttpStatus.OK);
            }
            throw new ExceptionTaskNotFound();
        }
        throw new ExceptionListNotFound();

    }

    @PostMapping("/lists/{id}") //добавление нового таска в список
    public ResponseEntity<?> newTask(@PathVariable("id") Long id, String title, String anons, Integer urgency) {

        Optional<List> listOp = todoRep.findById(id);

        if (listOp.isPresent()) {
            if (title == null || title.equals("")) {
                throw new ExceptionTitleCannotBeNull();
            } else if (anons == null || anons.equals("")) {
                throw new ExceptionAnonsCannotBeNull();
            } else if (urgency == null) {
                throw new ExceptionUrgencyCannotBeNull();
            }
            List list = listOp.get();
            Task newTask = new Task();
            newTask.setTitle(title);
            newTask.setAnons(anons);
            newTask.setUrgency(urgency);
            Date date = new Date();
            newTask.setDateCreating(date);
            newTask.setDateUpdating(date);
            list.setTasks(newTask);
            newTask.setList(list);
            taskRep.save(newTask);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            throw new ExceptionListNotFound();
        }
    }


    @PutMapping("/lists/{id}/{taskId}") //изменение дела в списке
    public ResponseEntity<?> updateTask(@PathVariable("id") Long id,
                                        @PathVariable("taskId") Long taskId,
                                        String title, String anons, Integer urgency) {

        Optional<List> listOp = todoRep.findById(id);
        Optional<Task> taskOp = taskRep.findById(taskId);

        Task task;
        if (listOp.isPresent()) {
            if (taskOp.isPresent()) {
                if (title == null || title.equals("")) {
                    throw new ExceptionTitleCannotBeNull();
                } else if (anons == null || anons.equals("")) {
                    throw new ExceptionAnonsCannotBeNull();
                } else if (urgency == null) {
                    throw new ExceptionUrgencyCannotBeNull();
                }
                task = taskOp.get();
            } else {
                throw new ExceptionTaskNotFound();
            }

            task.setTitle(title);
            task.setAnons(anons);
            task.setUrgency(urgency);
            task.setDateUpdating(new Date());
            taskRep.save(task);

            return new ResponseEntity<>(HttpStatus.OK);
        }
        throw new ExceptionListNotFound();
    }


    @DeleteMapping("/lists/{id}/{taskId}") //удаление таска
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteList(@PathVariable("id") Long id, @PathVariable("taskId") Long taskId) {
        if (todoRep.findById(id).isPresent()) {
            if (taskRep.findById(taskId).isPresent()) {
                taskRep.deleteById(taskId);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            throw new ExceptionTaskNotFound();
        } else {
            throw new ExceptionListNotFound();
        }
    }

    @PostMapping("/lists/{id}/markDone/{taskId}") //пометить список
    public ResponseEntity<?> newTask(@PathVariable("id") Long id,
                                     @PathVariable("taskId") Long taskId) {

        Optional<List> listOp = todoRep.findById(id);
        Optional<Task> taskOp = taskRep.findById(taskId);

        if (listOp.isPresent()) {
            if (taskOp.isPresent()) {
                Task task = taskOp.get();
                task.setStatus(!task.getStatus());
                taskRep.save(task);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            throw new ExceptionTaskNotFound();
        }
        throw new ExceptionListNotFound();
    }
}