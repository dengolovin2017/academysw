package com.todoList.todoList.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Title cannot be null!")
public class ExceptionTitleCannotBeNull extends RuntimeException{

}