package com.todoList.todoList.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;


@Entity
public class List {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 1, max = 50)
    private String title;

    private Date dateCreating;

    private Date dateUpdating;

    @OneToMany(mappedBy = "list", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private java.util.List<Task> tasks;

    public List() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateCreating() {
        return dateCreating;
    }

    public void setDateCreating(Date dateCreating) {
        this.dateCreating = dateCreating;
    }

    public Date getDateUpdating() {
        return dateUpdating;
    }

    public void setDateUpdating(Date dateUpdating) {
        this.dateUpdating = dateUpdating;
    }

    public java.util.List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Task task) {
        this.tasks.add(task);
    }

}
