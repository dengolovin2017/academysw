package com.todoList.todoList.controller;

import com.todoList.todoList.exceptions.ExceptionListNotFound;
import com.todoList.todoList.exceptions.ExceptionTitleCannotBeNull;
import com.todoList.todoList.model.List;
import com.todoList.todoList.rep.ListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

@RestController
public class TodoController {

    @Autowired
    private ListRepository todoRep;

    @GetMapping(value = "/lists")
    public ResponseEntity<?> getAllLists() {
        Iterable<List> listsIt = todoRep.findAll();
        ArrayList<List> lists = new ArrayList<>();

        for (List list : listsIt) {
            lists.add(list);
        }
        return new ResponseEntity<>(lists, HttpStatus.OK);
    }

    @GetMapping("/lists/{id}") //получение одного списка
    public ResponseEntity<?> getList(@PathVariable("id") Long id) {
        Optional<List> list = todoRep.findById(id);
        if (list.isPresent()) {
            return new ResponseEntity<>(todoRep.findById(id), HttpStatus.OK);
        } else {
            throw new ExceptionListNotFound();
        }
    }

    @DeleteMapping("/lists/{id}") //удаление списка
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteList(@PathVariable("id") Long id) {
        if (todoRep.findById(id).isPresent()) {
            todoRep.deleteById(id);
            return new ResponseEntity<>(id, HttpStatus.OK);
        } else {
            throw new ExceptionListNotFound();
        }
    }


    @PostMapping(value = "/lists") //добавить список
    public ResponseEntity<?> addList(String title) {

        if (title == null || title.equals("")) {
            throw new ExceptionTitleCannotBeNull();
        }
        List newList = new List();
        newList.setTitle(title);
        newList.setDateCreating(new Date());
        newList.setDateUpdating(new Date());
        todoRep.save(newList);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value = "/lists/{id}") //обновить список переименовать
    public ResponseEntity<?> updateList(@PathVariable("id") Long id, String title) {

        Optional<List> listOp = todoRep.findById(id);

        if (listOp.isPresent()) {
            if (title == null || title.equals("")) {
                throw new ExceptionTitleCannotBeNull();
            }
            List list = listOp.get();
            list.setTitle(title);
            list.setDateUpdating(new Date());
            todoRep.save(list);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            throw new ExceptionListNotFound();
        }
    }

//    @PostMapping(value = "/lists/{id}")
//    public Task postTasks(@PathVariable(value = "id") int id, Task task) {
//
//        List list = todoRep.findById(id).get();
//        list.setTasks(task);
//        todoRep.save(list);
//
//        return task;
//    }

//    @GetMapping(value = "/lists/{id}")
//    public java.util.List<Task> findAll(@PathVariable(value = "id") int id) {
//        Optional<List> listr = todoRep.findById(id);
//
//        java.util.List<Task> lists = listr.get().getTasks();
//
//        return lists;
//    }

//    @GetMapping(value = "/lists")  //сортировка
//    public java.util.List<List> sortAll() {
//        Iterable<List> listsIt = todoRep.findAll(Sort.by("title"));
//        ArrayList<List> lists = new ArrayList<>();
//
//        for (List list : listsIt) {
//            lists.add(list);
//        }
//        return lists;
//    }
}
