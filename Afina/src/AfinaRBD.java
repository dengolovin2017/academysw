/*
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AfinaRBD {

    private final int SIZE_ALPHABET = 32;
    private int a;
    private int b;
    private Integer[] keyset = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31};
    private final Character[] ALPHABET = {'s'};


    public AfinaRBD(int a, int b) {

        if (Arrays.asList(keyset).contains(a)) {
            this.a = a;
            this.b = b;
        } else {
            throw new IllegalArgumentException("Вы ввели неверный ключ!");
        }
    }

    public AfinaRBD(int b) {
        this.a = getKeyA();
        this.b = b;
    }

    private int getKeyA() {
        return (int) Math.random() * (keyset.length - 1);
    }

    private int getEncryptedNumber(int x, int a, int b) {
        return (a * x + b) % SIZE_ALPHABET;
    }

    public String encrypt(String text) {
        StringBuilder encryptedText = new StringBuilder();
        int encryptedNumber, a0 = a, a1 = a, a2 = a, b1 = b, b2 = b, b0 = b;
        char[] arrayText = text.toCharArray();
        for (int i = 0; i < text.length(); i++) {

            if (i >= 2) {
                a0 = (a1 * a2) % SIZE_ALPHABET;
                b0 = (b1 * b2) % SIZE_ALPHABET;
            }
            if (arrayText[i] == ' ') {
                encryptedText.append(' ');
            } else {
                encryptedNumber = getEncryptedNumber(Arrays.asList(ALPHABET).indexOf(arrayText[i]), a0, b0);
                encryptedText.append(ALPHABET[encryptedNumber]);
            }
        }
        return encryptedText.toString();
    }

//    private int getInverseNumber(int a) {
//
//        List<Integer[]> list = new ArrayList<>();
//        int alpha = a, beta = SIZE_ALPHABET, gama, nu = 1, omega = 1;
//
//        list.add(new Integer[]{alpha, beta, nu});
//        while (alpha != 1) {
//            gama = beta % alpha;
//            beta = alpha;
//            alpha = gama;
//            nu *= -1;
//            list.add(new Integer[]{alpha, beta, nu});
//        }
//        for (int i = list.size() - 1; i >= 0; i--) {
//            omega = (omega * list.get(i)[1] + list.get(i)[2]) / list.get(i)[0];
//        }
//        return omega;
//    }

    public String decrypt(String text) {
        StringBuilder decryptedText = new StringBuilder();
        char[] arrayText = text.toCharArray();
        int decryptedNumber, inverseNumber;
        int a0 = a, a1 = a, a2 = a, b1 = b, b2 = b, b0 = b;

        for (int i = 0; i < text.length(); i++) {

            if (i >= 2) {
                a0 = (a1 * a2) % SIZE_ALPHABET;
                b0 = (b1 * b2) % SIZE_ALPHABET;
            }
            inverseNumber = getInverseNumber(a0);
            if (arrayText[i] == ' ') {
                decryptedText.append(' ');
            } else {
                decryptedNumber = inverseNumber * (Arrays.asList(ALPHABET).indexOf(arrayText[i]) + SIZE_ALPHABET - b0)
                        %  SIZE_ALPHABET;
                decryptedText.append(ALPHABET[decryptedNumber]);
            }
        }
        return decryptedText.toString();
    }


}*/
