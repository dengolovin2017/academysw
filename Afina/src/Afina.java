import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Afina {

    private final int SIZE_ALPHABET = 32;
    private int a; 
    private int b;
    private Integer[] keyset = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31};
    private final Character[] ALPHABET = {'�', '�', '�', '�', '�', '�', '�', '�', 
    		'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�',
    		'�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�', '�'};


    public Afina(int a, int b) {

        if (Arrays.asList(keyset).contains(a)) {
            this.a = a;
            this.b = b;
        } else {
            throw new IllegalArgumentException("Вы ввели неверный ключ!");
        }
    }

    public Afina(int b) {
        this.a = getKeyA();
        this.b = b;
    }

    private int getKeyA() {
        return (int) Math.random() * (keyset.length - 1);
    }

    private int getEncryptedNumber(int x) {
        return (a * x + b) % SIZE_ALPHABET;
    }

    public String encrypt(String text) {
        StringBuilder encryptedText = new StringBuilder();
        int encryptedNumber;
        char[] arrayText = text.toCharArray();
        for (int i = 0; i < text.length(); i++) {
            if (arrayText[i] == ' ') {
                encryptedText.append(' ');
            } else {
                encryptedNumber = getEncryptedNumber(Arrays.asList(ALPHABET).indexOf(arrayText[i]));
                encryptedText.append(ALPHABET[encryptedNumber]);
            }
        }
        return encryptedText.toString();
    }




    private int getInverseNumber(int a) {

        List<Integer[]> list = new ArrayList<>();
        int alpha = a, beta = SIZE_ALPHABET, gama, nu = 1, omega = 1;
        list.add(new Integer[]{alpha, beta, nu});
        while (alpha != 1) {
            gama = beta % alpha;
            beta = alpha;
            alpha = gama;
            nu *= -1;
            list.add(new Integer[]{alpha, beta, nu});
        }
        for (int i = list.size() - 1; i >= 0; i--) {
            omega = (omega * list.get(i)[1] + list.get(i)[2]) / list.get(i)[0];
        }
        return omega;
    }



    public String decrypt(String text) {
        StringBuilder decryptedText = new StringBuilder();
        char[] arrayText = text.toCharArray();
        int decryptedNumber, inverseNumber = getInverseNumber(a);

        for (int i = 0; i < text.length(); i++) {
            if (arrayText[i] == ' ') {
                decryptedText.append(' ');
            } else {
                decryptedNumber = inverseNumber * (Arrays.asList(ALPHABET).indexOf(arrayText[i]) + SIZE_ALPHABET - b)
                        %  SIZE_ALPHABET;
                decryptedText.append(ALPHABET[decryptedNumber]);
            }
        }
        return decryptedText.toString();
    }

    public String[] cryptoanlis(String text) {
        int a, b, decryptedNumber, inverseNumber, it = 0;
        String[] arr = new String[keyset.length * SIZE_ALPHABET];
        StringBuilder decryptedText;
        char[] arrayText;
        for (int i = 0; i < keyset.length; i++) {
            a = keyset[i];
            for (int j = 1; j < 33; j++) {
                b = j;
                decryptedText = new StringBuilder();
                arrayText = text.toCharArray();
                inverseNumber = getInverseNumber(a);

                for (int k = 0; k < text.length(); k++) {
                    if (arrayText[k] == ' ') {
                        decryptedText.append(' ');
                    } else {
                        decryptedNumber = inverseNumber * (Arrays.asList(ALPHABET).indexOf(arrayText[k]) + SIZE_ALPHABET - b)
                                % SIZE_ALPHABET;
                        decryptedText.append(ALPHABET[decryptedNumber]);
                    }
                }
                arr[it++] = decryptedText.toString();
            }
        }
        return arr;
    }

}
