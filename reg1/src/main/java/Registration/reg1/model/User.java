package Registration.reg1.model;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID id;

    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "login")
    private String login;

    @NotNull
    @Size(min = 5, max = 100)
    @Column(name = "email")
    private String email;

    @NotNull
    @Size(min = 4, max = 100)
    @Column(name = "password")
    private String password;

    @Transient
    private String confirmPassword;

    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "company")
    private String company;

    @Size(max = 500)
    @Column(name = "company_description")
    private String companyDescription;

    @NotNull
    @Size(max = 15)
    @Column(name = "tin")
    private String tin;

    @NotNull
    @Size(max = 30)
    @Column(name = "phone_number")
    private String phoneNumber;

    @NotNull
    @Size(max = 200)
    @Column(name = "address")
    private String address;

    @NotNull
    @Size(max = 100)
    @Column(name = "specialization")
    private String specialization;

    @NotNull
    @Column(name = "status")
    private boolean status;

    @ManyToMany
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
}
