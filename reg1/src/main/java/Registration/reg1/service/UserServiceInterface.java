package Registration.reg1.service;

import Registration.reg1.model.User;

public interface UserServiceInterface {

    void save(User user);

    User findByLogin(String login);
}
