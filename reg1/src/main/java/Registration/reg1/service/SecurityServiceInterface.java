package Registration.reg1.service;

public interface SecurityServiceInterface {

    String findLoggedUser();

    void autoLogin(String login, String password);
}
