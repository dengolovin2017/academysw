package Registration.reg1.service;

import Registration.reg1.model.Role;
import Registration.reg1.model.User;
import Registration.reg1.repository.RoleRepository;
import Registration.reg1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService implements UserServiceInterface {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private String performer = "e6739d54-7c9d-46d4-8c93-d2b3150bd7c2";

//    private String customer = "6f238a45-fe4c-4162-b076-4c41caeb2ad0";
//
//    private String admin = "0ccfbcfe-4f45-40e3-bd44-68576914cbb3";

    @Override
    public void save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findById(UUID.fromString(performer)).get());
        user.setRoles(roles);
        userRepository.save(user);
    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findAllByLogin(login);
    }
}