package Ciphers;

import java.util.ArrayList;
import java.util.List;

public class RecurrentHill {


    private final static int SIZE_ALPHABET = 37;
    private final static String ALPHABET = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя., ?";
    private static final int[][] KEY_FINAL = {{0, 12, 29}, {16, 9, 14}, {9, 8, 13}};
    private static int[][] KEY = {{0, 12, 29}, {16, 9, 14}, {9, 8, 13}};
    private final static int KEY_LENGTH = 3;




    public static void setKey(String key) {

        if (key == null || key.equals(" ")) {
            return;
        }
        char[] chars = key.toCharArray();
        int k = 0;
        System.out.println("Ваш ключ:\n");
        for (int i = 0; i < KEY_LENGTH; i++) {
            for (int j = 0; j < KEY_LENGTH; j++) {
                KEY[i][j] = ALPHABET.indexOf(chars[k++]);
                System.out.print(KEY[i][j] + " ");
            }
            System.out.println();
        }

        if (getDetermine() <= 0) {
            KEY = KEY_FINAL;
        }
    }

    public static String decrypt(String text) {

        int numberBlocks = text.length() / KEY_LENGTH;
        int[][] blocks = encodeNumber2(numberBlocks, text); //кодирование текста в числа
        int[][] reKeyMatrix = getSpecialMatrix();

        return getText2(blocks, reKeyMatrix);
    }

    private static String getText2(int[][] blocks, int[][] matrix) {
        for (int j = 0; j < blocks.length; j++) {
            blocks[j] = matrixMultiplication(blocks[j], matrix); //кодирование блоков + % 37
        }
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < blocks.length; j++) {
            for (int k = 0; k < KEY_LENGTH; k++) {
                builder.append(ALPHABET.charAt(blocks[j][k]));

            }
        }
        return builder.toString();
    }


    private static int[][] encodeNumber2(int numberBlocks, String text) {//кодирование текста в числа

        int[][] blocks = new int[numberBlocks][KEY_LENGTH];
        int i = 0, promo;
        for (int j = 0; j < numberBlocks; j++) {
            for (int k = 0; k < KEY_LENGTH; k++) {
                promo = ALPHABET.indexOf(text.charAt(i++)) - j;

                while (promo < 0) {
                    promo += SIZE_ALPHABET;
                }
                blocks[j][k] = promo;
            }

        }
        return blocks;
    }

    public static String encrypt(String text) {

        if (text.length() % KEY_LENGTH == 1) {
            text += "  ";
        } else if (text.length() % KEY_LENGTH == 2) {
            text += " ";
        }
        int numberBlocks = text.length() / KEY_LENGTH;

        int[][] blocks = encodeNumber(numberBlocks, text); //кодирование текста в числа

        return getText(blocks, KEY);
    }




    private static int[][] encodeNumber(int numberBlocks, String text) {//кодирование текста в числа

        int[][] blocks = new int[numberBlocks][KEY_LENGTH];
        int i = 0;
        for (int j = 0; j < numberBlocks; j++) {
            for (int k = 0; k < KEY_LENGTH; k++) {
                blocks[j][k] = ALPHABET.indexOf(text.charAt(i++));
            }
        }
        return blocks;
    }


    private static String getText(int[][] blocks, int[][] matrix) {
        for (int j = 0; j < blocks.length; j++) {
            blocks[j] = matrixMultiplication(blocks[j], matrix); //кодирование блоков + % 37
            for (int i = 0; i < KEY_LENGTH; i++) {
                blocks[j][i] += j;
                if (blocks[j][i] >= SIZE_ALPHABET) {
                    while (blocks[j][i] >= SIZE_ALPHABET) {
                        blocks[j][i] -= SIZE_ALPHABET; //прибавляет +1 рекурентно
                    }
                }
            }
        }
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < blocks.length; j++) {
            for (int k = 0; k < KEY_LENGTH; k++) {
                builder.append(ALPHABET.charAt(blocks[j][k]));

            }
        }
        return builder.toString();
    }

    private static int[] matrixMultiplication(int[] block, int[][] key) {
        int[] result = new int[KEY_LENGTH];

        for (int i = 0; i < KEY_LENGTH; i++) {
            int res = 0;
            for (int j = 0; j < KEY_LENGTH; j++) {
                res += block[j] * key[j][i];
            }
            result[i] = res % SIZE_ALPHABET;
        }
        return result;
    }


    private static int getDetermine() {
        return KEY[0][0] * KEY[1][1] * KEY[2][2]
                + KEY[0][1] * KEY[1][2] * KEY[2][0]
                + KEY[1][0] * KEY[2][1] * KEY[0][2]
                - KEY[0][2] * KEY[1][1] * KEY[2][0]
                - KEY[0][1] * KEY[1][0] * KEY[2][2]
                - KEY[0][0] * KEY[1][2] * KEY[2][1];
    }

    private static int[][] getSpecialMatrix() {

        int[][] specialMatrix = new int[KEY_LENGTH][KEY_LENGTH];
        int inverse = -1;

        for (int i = 0; i < KEY_LENGTH; i++) {
            for (int j = 0; j < KEY_LENGTH; j++) {
                specialMatrix[i][j] = ((((int) Math.pow(inverse, i + j + 2) * getDet2(i, j))
                        % SIZE_ALPHABET) * getInverseNumber()) % SIZE_ALPHABET;
                if (specialMatrix[i][j] < 0) {
                    specialMatrix[i][j] += SIZE_ALPHABET;
                }
            }
        }
        specialMatrix = transposition(specialMatrix);

        return specialMatrix;
    }

    private static int[][] transposition(int[][] matrix) {

        int[][] transpositionMatrix = new int[KEY_LENGTH][KEY_LENGTH];

        for (int i = 0; i < KEY_LENGTH; i++) {
            for (int j = 0; j < KEY_LENGTH; j++) {
                transpositionMatrix[i][j] = matrix[j][i];
            }
        }
        return transpositionMatrix;
    }

    private static int getDet2(int i, int j) {
        int tempA, tempB = 0;
        int[][] matrix = new int[2][2];

        for (int k = 0; k < KEY_LENGTH; k++) {
            if (i != k) {
                tempA = 0;
                for (int l = 0; l < KEY_LENGTH; l++) {
                    if (l != j) {
                        matrix[tempB][tempA] = KEY[k][l];
                        tempA++;
                    }
                }
                tempB++;
            }
        }
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    }

    private static int getInverseNumber() {

        List<Integer[]> list = new ArrayList<>();
        int alpha = getDetermine(), beta = SIZE_ALPHABET, gama, nu = 1, omega = 1;
        list.add(new Integer[]{alpha, beta, nu});
        while (alpha != 1) {
            gama = beta % alpha;
            beta = alpha;
            alpha = gama;
            nu *= -1;
            list.add(new Integer[]{alpha, beta, nu});
        }
        for (int i = list.size() - 1; i >= 0; i--) {
            omega = (omega * list.get(i)[1] + list.get(i)[2]) / list.get(i)[0];
        }
        return omega;
    }
}
