import Ciphers.HillAPI;
import Ciphers.RecurrentHill;

import java.io.*;

public class Main {

    private static int n = 34, count = 0;
    private static char[] charArr = new char[n];
    private static int[] counterArr = new int[n];

    public static void main(String[] args) throws IOException {


        String a = "пугало";
        System.out.println(HillAPI.encrypt(a));
        System.out.println(HillAPI.decrypt("ь.уяыл"));

        System.out.println(RecurrentHill.encrypt(a));
        System.out.println(RecurrentHill.decrypt("нщёяжю"));








        //frequency("resurces/text.txt"); //частоты основного текста
        /**
        String shiphr = getText("text.txt");
        String encryptText = HillAPI.encrypt(shiphr);
        createFile(encryptText);
        frequency("resurces/1.txt", "1.log"); //частоты основного текста
        frequency("resurces/2.txt", "2.log");
        frequency("resurces/3.txt", "3.log");
        frequency("resurces/text.txt", "text.log");
         */

        //"кот жил, и собака жила, а ворона... тоже жила"
//        String ALPHABET = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя., ?";
//        for (int i = 0; i < ALPHABET.length(); i++) {
//            System.out.println((int) ALPHABET.charAt(i));
//        }
//        System.out.println();
//        String shiphr = "ккккккккк";
////
//        System.out.println(HillAPI.encrypt(shiphr));
////
//        System.out.println(HillAPI.decrypt("дл?заизъфтлцпдете,эвдзюёц  аирбшфькчжлбючцжюх"));
//
//        //RecurrentHill.setKey("ключикжелтый");
//        System.out.println(RecurrentHill.encrypt(shiphr));
//        System.out.println(RecurrentHill.decrypt("дл?ибййьцхощузичйв?зйобмюёёисщк югх тчмжа?фз?"));



    }

    private static String getText(String file1) {

        try {
            File file = new File("resurces/" + file1);

            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            StringBuilder builder = new StringBuilder();
            String line = reader.readLine();
            builder.append(line);
            while (line != null) {
                line = reader.readLine();
                builder.append(line);
            }
            String text = builder.toString();
            StringBuilder b = new StringBuilder();
            String ALPHABET = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя., ?";
            for (int i = 0; i < text.length(); i++) {
                if (ALPHABET.contains(String.valueOf(text.charAt(i)))) {
                    b.append(text.charAt(i));
                }
            }

            return b.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    private static void createFile(String content) throws IOException {
        FileWriter writer1 = new FileWriter("resurces/1.txt");
        FileWriter writer2 = new FileWriter("resurces/2.txt");
        FileWriter writer3 = new FileWriter("resurces/3.txt");
        FileWriter writer = new FileWriter("resurces/encr.txt");

        writer.write(content);
        for (int i = 0; i < content.length(); i+=3) {
            writer1.write(content.charAt(i));
        }
        for (int i = 1; i < content.length(); i+=3) {
            writer2.write(content.charAt(i));
        }
        for (int i = 2; i < content.length(); i+=3) {
            writer3.write(content.charAt(i));
        }
        writer1.close();
        writer2.close();
        writer3.close();
        writer.close();
    }

    private static void frequency(String path, String secondFile) {
        try (FileReader reader = new FileReader(path)) {
            int c;
            count = 0;
            while ((c = reader.read()) != -1) {
                if (c <= 1103 && c >= 1072) {
                    counterArr[c - 1072]++;
                    count++;
                } else if (c == 1105) {
                    counterArr[32]++;
                    count++;
                } else if (c == 32) {
                    counterArr[33]++;
                    count++;
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        try (FileWriter writer = new FileWriter("resurces/" + secondFile, false)) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < n; i++) {
                if (i + 1072 == 1104) {
                    charArr[i] = (char) (1105);
                    builder.append((char) (1105) + " - " + counterArr[i] + " - " + ((double) counterArr[i] / count) + "\n");
                } else if (i + 1072 == 1105) {
                    charArr[i] = (char) (32);
                    builder.append((char) (32) + " - " + counterArr[i] + " - " + ((double) counterArr[i] / count) + "\n");
                } else {
                    charArr[i] = (char) (i + 1072);
                    builder.append((char) (i + 1072) + " - " + counterArr[i] + " - " + ((double) counterArr[i] / count) + "\n");
                    //System.out.println((char) (i + 65) + " - " + counterArr[i]);
                }
            }
            builder.append("\nКол-во букв - " + count);
            writer.write(builder.toString());
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
