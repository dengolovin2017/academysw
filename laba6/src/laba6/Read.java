package laba6;

import java.io.IOException;

public class Read {
	
	private static byte b[]; 
	
	public static String enterString() {
		b = new byte[1024];
		try {
			System.in.read(b);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String s = new String(b);
		return s.trim();
	}
	
	public static Integer enterNumber() {
		b = new byte[1024];
		Integer i = null;
		try {
			System.in.read(b);
			String s = new String(b);
			i = Integer.parseInt(s.trim());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return i;
	}

}
