package laba6;

import java.io.Serializable;

public class Client  implements Serializable {
	
	private String name;
	private int phoneNumber;
	
	public Client() {
		name = "";
		phoneNumber = 0;
	}
	
	public Client(String name, int phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
