package laba2;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MotorShow {
	
	private String name;
	private List<Car> cars;
	private Scanner in;
	
	public MotorShow(String name) {
		this.name = name;
		cars = new ArrayList<>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void add(Car car) {
		cars.add(car);
	}
	
	public void remove(Car car) {
		cars.remove(car);
	}
	
	public List<Car> get() {
		return cars;
	}	
	
	public String createRequestStand(RequestToBuy request) { 

		RequestFromStand r = new RequestFromStand(request.getFullName(), request.getPhoneNumber());
		
		
		if (cars.size() == 0) {
			try {
				throw new KolichestvoAvtomobileyException("����������� ���!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		}
		
		
		double price = -1;
		int number = -1;
		
		
		System.out.println("\n�������� ����� ����\n");
		for (int i = 0; i < cars.size(); i++) {
			System.out.println((i + 1) + ") " + cars.get(i));
		}
		for(;;) {	
			in = new Scanner(System.in);
			try {
				number = in.nextInt();
				if (cars.get(number - 1).isAvailability()) {
					cars.get(number - 1).decrementStockAmount();
					price = cars.get(number - 1).getPrice();
				}
				break;
			} catch (InputMismatchException e) {
				System.err.println("�� ����� �� �����!");
				e.printStackTrace();
			} catch (Exception e) {
				System.err.println("�� ����� �������� �����!");
				e.printStackTrace();	
			}
		}
			
		r.setPrice(price);
		r.calculateCost();
		if (price <= 0) {
			try {
				throw new KolichestvoAvtomobileyException("���������� ������ ����� �����������!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		} else {
			return "�� ������ " + cars.get(number - 1).getCarBrand() + " �� " + price + " ���";
		}
	}
	
	public String createRequestDelivery(RequestToBuy request, int sale) { 

		RequestDeferredDelivery r = new RequestDeferredDelivery(request.getFullName(), request.getPhoneNumber(), sale);
		
		if (cars.size() == 0) {
			try {
				throw new KolichestvoAvtomobileyException("����������� ���!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		}
		
		double price = -1;
		int number;
		
		System.out.println("\n�������� ����� ����\n");
		for (int i = 0; i < cars.size(); i++) {
			System.out.println((i + 1) + ") " + cars.get(i));
		}
		
		for(;;) {	
			in = new Scanner(System.in);
			try {
				number = in.nextInt();
				if (cars.get(number - 1).isAvailability()) {
					cars.get(number - 1).decrementStockAmount();
					price = cars.get(number - 1).getPrice();
				}
				break;
			} catch (InputMismatchException e) {
				System.err.println("�� ����� �� �����!");
				e.printStackTrace();
			} catch (Exception e) {
				System.err.println("�� ����� �������� �����!");
				e.printStackTrace();	
			}
		}
		
		r.setPrice(price);
		price = r.calculateCost();
		if (price <= 0) {
			try {
				throw new KolichestvoAvtomobileyException("���������� ������ ����� �����������!");
			} catch(KolichestvoAvtomobileyException e) {
				e.printStackTrace();
			}
			return "";
		} else {
			return "�� ������ " + cars.get(number - 1).getCarBrand() + " �� " + price + " ��� �� ������� " + sale + "%";
		}
	}
}
