package laba2;

public class Car {
	
	private String carBrand;
	private int maxNumberPass;
	private double price;
	private int stockAmount;
	private boolean isAvailability;
	
	public Car(String carBrand, int maxNumberPass, double price, int stockAmount) {
		this.carBrand = carBrand;
		this.maxNumberPass = maxNumberPass;
		this.price = price;
		this.stockAmount = stockAmount;
		
		if (stockAmount > 0) {
			this.isAvailability = true;
		}
	}
	
	public String getCarBrand() {
		return carBrand;
	}
	
	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}
	
	public int getMaxNumberPass() {
		return maxNumberPass;
	}
	
	public void setMaxNumberPass(int maxNumberPass) {
		this.maxNumberPass = maxNumberPass;
	}
	
	public int getStockAmount() {
		return stockAmount;
	}
	
	public void decrementStockAmount() {
		stockAmount--;
		if (stockAmount == 0) {
			isAvailability = false;
		}
	}
	
	public void setStockAmount(int stockAmount) {
		this.stockAmount = stockAmount;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public boolean isAvailability() {
		return isAvailability;
	}
	
	public void setAvailability(boolean isAvailability) {
		this.isAvailability = isAvailability;
	}
	
	public String toString() {
		return carBrand + " - ���� ���� - " + maxNumberPass + " - ���� - " + price + " - ���-�� �� ������ - "  +  stockAmount;
	}

}
