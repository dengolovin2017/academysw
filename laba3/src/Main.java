import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
	
	//private static String f1 = "data/f1.txt";
	private static String f2 = "data/f2.txt";
	private static String f3 = "data/f3.txt";
	private static String f4 = "data/f4.txt";
	private static int[] countWord = new int[30];
	private static int[] countChar = new int[30];
	
	public static void main(String[] args) throws IOException {
		
		FileInputStream fileIn = null;
        FileOutputStream fileOut = null, fileOut2 = null, fileOut3 = null;

        try {
            fileIn = new FileInputStream("data\\f1.txt");
            fileOut = new FileOutputStream("data\\f4.txt");
            fileOut2 = new FileOutputStream("data\\f2.txt");
            fileOut3 = new FileOutputStream("data\\f3.txt");

            int a, i = 0;
            while ((a = fileIn.read()) != -1) {
            	
            	if (a == 13) {
            		fileOut2.write(Integer.toString(countChar[i]).getBytes());
            		fileOut2.write(13);
            		fileOut2.write(10);
            		fileOut3.write(Integer.toString(countWord[i]).getBytes());
            		fileOut3.write(13);
            		fileOut3.write(10);
            		fileOut.write(32);
            		fileOut.write(Integer.toString(countChar[i]).getBytes());
            		fileOut.write(32);
            		countWord[i]++;
            		fileOut.write(Integer.toString(countWord[i]).getBytes());
            		fileOut.write(a);
            		i++;
            	} else {
            		fileOut.write(a);
            		if (a == 32) {
            		countWord[i]++;
            		} 
            	
            		if (a != 13 || a != 10 || a != 32) {
            		countChar[i]++;
            		}
            	}
            	System.out.println(a);
                
            }
        } finally {
            if (fileIn != null) {
                fileIn.close();
            }
            if (fileOut != null) {
                fileOut.close();
            }
            if (fileOut2 != null) {
                fileOut.close();
            }
            if (fileOut3 != null) {
                fileOut.close();
            }
        }
    }

}
