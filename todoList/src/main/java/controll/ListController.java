package controll;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Date;
import java.util.Optional;

@RestController
public class ListController {

    @Autowired
    private ListRepository rep;

    @GetMapping("/lists/{id}")
    public ResponseEntity<?> getList(@PathVariable("id") Long id) {
        Optional<List> list = rep.findById(id);
        if (list.isPresent()) {
            return new ResponseEntity<>(rep.findById(id), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(-1, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/")
    public String getList() {
        return (new Date()).toString();
    }

    @DeleteMapping("/lists/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteList(@PathVariable("id") Long id) {
        if (rep.findById(id).isPresent()) {
            rep.deleteById(id);
            return new ResponseEntity<>(id, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(-1, HttpStatus.BAD_REQUEST);
        }
    }

//    @GetMapping("/lists")
//    public ResponseEntity<?> getLists(@RequestBody Map<String, String> params) {
//        // Проверки и прочее.
//        long startId = 1L;
//        if (params.containsKey("startId")) {
//            long parseStartId = Long.parseLong(params.get("startId"));
//            if (parseStartId > 0L) {
//                startId = parseStartId;
//            }
//        }
//        int limit = 10;
//        if (params.containsKey("limit")) {
//            int parseLimit = Integer.parseInt(params.get("limit"));
//            if (parseLimit > 0 && parseLimit < 100) {
//                limit = parseLimit;
//            }
//        }
//
//        Iterable<TaskList> oTaskList = repository.findAll();
//        Iterator<TaskList> listIterator = oTaskList.iterator();
//        List<Map<String, String>> taskListDate = new ArrayList<>();
//
//        while (listIterator.hasNext() && limit > 0) {
//            TaskList taskList = listIterator.next();
//            if (taskList.getId() >= startId) {
//                Map<String, String> dataMap = new HashMap<>();
//                dataMap.put("id", Long.toString(taskList.getId()));
//                dataMap.put("name", taskList.getName());
//                dataMap.put("dateCreated", taskList.getDateCreated().toString());
//                dataMap.put("dateChange", taskList.getDateChange().toString());
//                dataMap.put("isDone", Boolean.toString(taskList.isDone()));
//                taskListDate.add(dataMap);
//                limit--;
//            }
//        }
//        return new ResponseEntity<>(taskListDate, HttpStatus.OK);
//    }


}
