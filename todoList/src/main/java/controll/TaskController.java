package controll;


import model.List;
import model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rep.ListRepository;
import rep.TaskRepository;

import java.util.Date;
import java.util.Optional;

@RestController
public class TaskController {

    @Autowired
    private ListRepository listRepository;
    @Autowired
    private TaskRepository taskRepository;


    @PostMapping("/lists/{id}")
    public ResponseEntity<?> newTask(@PathVariable("id") Long id,
                                     @RequestBody String title,
                                     @RequestBody String anons,
                                     @RequestBody int urgency) {
        // Проверки
        // создание и сохранение в БД
        Optional<List> list = listRepository.findById(id);

        if (list.isPresent()) {
            List newList = list.get();
            Task newTask = new Task();
            newTask.setTitle(title);
            newTask.setAnons(anons);
            newTask.setUrgency(urgency);
            Date date = new Date();
            newTask.setDateCreating(date);
            newTask.setDateUpdating(date);
            newTask.setList(newList);
            String taskId = taskRepository.save(newTask).getId().toString();
            return new ResponseEntity<>(taskId, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
