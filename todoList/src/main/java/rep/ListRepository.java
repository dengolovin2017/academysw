package rep;


import org.springframework.data.repository.CrudRepository;
import model.List;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

@Transactional
@Repository
public interface ListRepository extends CrudRepository<List, Long> {
}
