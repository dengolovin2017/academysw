package model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="list_id")
    @JsonIgnore
    private List list;

    private Date dateCreating;

    private Date dateUpdating;

    private String title;

    private String anons;

    private int urgency;

    private boolean status;

    public Task() {
    }

    public Task(List listId, Date dateCreating, Date dateUpdating, String title, String anons, int urgency, boolean status) {
        this.list = list;
        this.dateCreating = dateCreating;
        this.dateUpdating = dateUpdating;
        this.title = title;
        this.anons = anons;
        this.urgency = urgency;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public Date getDateCreating() {
        return dateCreating;
    }

    public void setDateCreating(Date dateCreating) {
        this.dateCreating = dateCreating;
    }

    public Date getDateUpdating() {
        return dateUpdating;
    }

    public void setDateUpdating(Date dateUpdating) {
        this.dateUpdating = dateUpdating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnons() {
        return anons;
    }

    public void setAnons(String anons) {
        this.anons = anons;
    }

    public int getUrgency() {
        return urgency;
    }

    public void setUrgency(int urgency) {
        this.urgency = urgency;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
