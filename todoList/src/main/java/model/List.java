package model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class List {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String title;

    private Date dateCreating;

    private Date dateUpdating;

    @OneToMany(mappedBy = "list", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private java.util.List<Task> tasks;

    public List() {
    }

    public List(Long id, String title, Date dateCreating, Date dateUpdating, java.util.List<Task> tasks) {
        this.id = id;
        this.title = title;
        this.dateCreating = dateCreating;
        this.dateUpdating = dateUpdating;
        this.tasks = tasks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDateCreating() {
        return dateCreating;
    }

    public void setDateCreating(Date dateCreating) {
        this.dateCreating = dateCreating;
    }

    public Date getDateUpdating() {
        return dateUpdating;
    }

    public void setDateUpdating(Date dateUpdating) {
        this.dateUpdating = dateUpdating;
    }

    public java.util.List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Task task) {
        this.tasks.add(task);
    }
}
