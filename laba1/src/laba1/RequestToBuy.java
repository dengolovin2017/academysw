package laba1;

public class RequestToBuy {
	
	protected String fullName;
	protected int phoneNumber;
	protected double price;
	
	public RequestToBuy(String fullName, int phoneNumber) {
		if (fullName.equals("") || fullName == null) {
			try {
				throw new FIOException("���������� ������� ������ - �� ������� ��� ����������");
			} catch(FIOException e) {
				e.printStackTrace();
			}
		}
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		price = 0;

	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	protected double calculateCost() {
		return price;
	}
}
