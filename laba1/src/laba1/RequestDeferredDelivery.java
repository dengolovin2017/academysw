package laba1;

class RequestDeferredDelivery extends RequestToBuy{
	
	private int sale = 0;
	
	public RequestDeferredDelivery(String fullName, int phoneNumber, int sale) {
		super(fullName, phoneNumber);
		this.sale = sale;
	}

	
	public double calculateCost() {

		return price - (price / 100) * sale;
	}

	public int getSale() {
		return sale;
	}

	public void setSale(int sale) {
		this.sale = sale;
	}

}
