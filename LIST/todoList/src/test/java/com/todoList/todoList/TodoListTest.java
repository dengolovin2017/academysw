package com.todoList.todoList;

import com.todoList.todoList.controller.TodoController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-list-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class TodoListTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoController listController;

    @Test
    void todoListControllerAutowiredTest() throws Exception {
        assertThat(listController).isNotNull();
    }

    @Test
    void getAllListWithoutSortingTest() throws Exception {
        this.mockMvc.perform(get("/list"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getAllListWithSortingTest() throws Exception {
        this.mockMvc.perform(get("/lists")
                .param("direction", "asc")
                .param("sortValue", "dateCreating")
                .param("numberRecordsOnPage", "2")
                .param("from", "01/01/2000 15:00")
                .param("to", "01/01/2030 15:00"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getAllListWithSortingBadTest() throws Exception {
        this.mockMvc.perform(get("/lists")
                .param("direction", "???")
                .param("sortValue", "???")
                .param("numberRecordsOnPage", "???")
                .param("from", "01/01/2000 15:00")
                .param("to", "01/01/2030 15:00"))
                .andDo(print())
                .andExpect(status().is(400));
    }

	@Test
	void getListTest() throws Exception {
		this.mockMvc.perform(get("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e"))
				.andDo(print())
				.andExpect(status().isOk());
	}

    @Test
    void getListNotFoundTest() throws Exception {
        this.mockMvc.perform(get("/lists/2d6adfc6-39a4-4c78-a39c-5f08c126db4e"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteListTest() throws Exception {
        this.mockMvc.perform(delete("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteListNotFoundTest() throws Exception {
        this.mockMvc.perform(delete("/lists/2d6adfc6-39a4-4c78-a39c-5f08c126db4e"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void addListTest() throws Exception {
        this.mockMvc.perform(post("/lists")
                .param("title", "newList"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void addListTitleIsEmptyTest() throws Exception {
        this.mockMvc.perform(post("/lists")
                .param("title", ""))
                .andDo(print())
                .andExpect(status().is(400));
    }

    @Test
    void updateListTest() throws Exception {
        this.mockMvc.perform(put("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e")
                .param("title", "new Title"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void updateListNotFoundTest() throws Exception {
        this.mockMvc.perform(put("/lists/2d6adfc6-39a4-1c78-a39c-5f08c876db5e")
                .param("title", "new Title"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}
