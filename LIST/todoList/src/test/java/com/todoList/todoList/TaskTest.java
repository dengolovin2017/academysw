package com.todoList.todoList;

import com.todoList.todoList.controller.TaskController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-list-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class TaskTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskController taskController;

    @Test
    void taskControllerAutowiredTest() throws Exception {
        assertThat(taskController).isNotNull();
    }

    @Test
    void getTaskTest() throws Exception {
        this.mockMvc.perform(get("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/2d6adfc6-39a4-4c78-a39c-1f03c876db5a"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getTaskNotFoundTest() throws Exception {
        this.mockMvc.perform(get("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/3d6adfc6-39a4-4c78-a39c-1f03c876db5a"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void addTaskTest() throws Exception {
        this.mockMvc.perform(post("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e")
                .param("title", "newTask")
                .param("anons", "new Anons")
                .param("urgency", "5"))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void addTaskBadRequestTest() throws Exception {
        this.mockMvc.perform(post("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e")
                .param("title", "newTask")
                .param("urgency", "5"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void addTaskNotFoundTest() throws Exception {
        this.mockMvc.perform(post("/lists/3d6adfc6-39a4-4c78-a39c-5f08c876db2e")
                .param("title", "newTask")
                .param("anons", "new Anons")
                .param("urgency", "5"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void updateTaskTest() throws Exception {
        this.mockMvc.perform(put("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/2d6adfc6-39a4-4c78-a39c-1f03c876db5a")
                .param("title", "newTask1")
                .param("anons", "new Anons1")
                .param("urgency", "3"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void updateTaskBadRequestTest() throws Exception {
        this.mockMvc.perform(put("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/2d6adfc6-39a4-4c78-a39c-1f03c876db5a")
                .param("title", "newTask1")
                .param("anons", "new Anons1"))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateTaskNotFoundTest() throws Exception {
        this.mockMvc.perform(put("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/2d6adfc6-19a4-4c78-a39c-1f03c876db5a")
                .param("title", "newTask1")
                .param("anons", "new Anons1")
                .param("urgency", "3"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteTaskTest() throws Exception {
        this.mockMvc.perform(delete("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/2d6adfc6-39a4-4c78-a39c-1f03c876db5a"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteTaskNotFoundTest() throws Exception {
        this.mockMvc.perform(delete("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/4d6adfc6-39a4-4c78-a39c-1f03c876db5a"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void markTaskTest() throws Exception {
        this.mockMvc.perform(post("/lists/2d6adfc6-39a4-4c78-a39c-5f08c876db2e/markDone/2d6adfc6-39a4-4c78-a39c-1f03c876db5a"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void markTaskNotFoundTest() throws Exception {
        this.mockMvc.perform(post("/lists/2d6adfc6-39a4-4c68-a39c-5f08c876db2e/markDone/1d6adfc6-39a4-4c78-a39c-1f03c876db5a"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}
