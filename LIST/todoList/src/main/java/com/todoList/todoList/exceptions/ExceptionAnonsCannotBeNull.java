package com.todoList.todoList.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ExceptionAnonsCannotBeNull - класс исключение
 * Указывает, что текст задачи не может быть пустым
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Anons cannot be null!")
public class ExceptionAnonsCannotBeNull extends RuntimeException {

}