package com.todoList.todoList.model;

import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

/**
 * List - класс сущность списка задач
 */
@Entity
@Data
@ApiModel(value = "class List")
public class List {

    /**
     * id списка
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id")
    private UUID id;

    /**
     * Название списка
     */
    @NotNull
    @Size(min = 1, max = 50)
    @ApiModelProperty(value = "list title")
    private String title;

    /**
     * Дата создания списка
     */
    @ApiModelProperty(value = "list date creating")
    private Date dateCreating;

    /**
     * Дата изменения списка
     */
    @ApiModelProperty(value = "list date updating")
    private Date dateUpdating;

    /**
     * Задачи в данном списке
     */
    @OneToMany(mappedBy = "list", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ApiModelProperty(value = "tasks")
    private java.util.List<Task> tasks;

    /**
     * Метод добавления задачи
     */
    public void setTasks(Task task) {
        this.tasks.add(task);
    }

}
