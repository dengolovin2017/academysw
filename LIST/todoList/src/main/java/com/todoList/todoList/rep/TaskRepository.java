package com.todoList.todoList.rep;

import com.todoList.todoList.model.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.UUID;

/**
 * TaskRepository - интерфейс репозиторий для задач (Task)
 */
@Transactional
@Repository
public interface TaskRepository extends CrudRepository<Task, UUID> {
}
