package com.todoList.todoList.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ExceptionTaskNotFound - класс исключение
 * Указывает, что задача не найдена
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Task not found!")
public class ExceptionTaskNotFound extends RuntimeException{

}
