package com.todoList.todoList.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

/**
 * List - класс сущность задачи
 */
@Entity
@Data
@ApiModel(value = "class Task")
public class Task {

    /**
     * id списка
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "id")
    private UUID id;

    /**
     * Список, которому принадлежит задача
     */
    @ManyToOne
    @JoinColumn(name="list_id")
    @JsonIgnore
    @ApiModelProperty(value = "list")
    private List list;

    /**
     * Дата создания задачи
     */
    @ApiModelProperty(value = "task date creating")
    private Date dateCreating;

    /**
     * Дата изменения задачи
     */
    @ApiModelProperty(value = "task date updating")
    private Date dateUpdating;

    /**
     * Статус задачи (выполнена, невыполнена)
     */
    @ApiModelProperty(value = "task status")
    private boolean status;

    /**
     * Название задачи
     */
    @NotNull
    @Size(min = 1, max = 50)
    @ApiModelProperty(value = "task name")
    private String title;

    /**
     * Текст задачи
     */
    @NotNull
    @Size(min = 1, max = 250)
    @ApiModelProperty(value = "task anons")
    private String anons;

    /**
     * Важность задачи
     */
    @ApiModelProperty(value = "task priority")
    private int urgency;
}