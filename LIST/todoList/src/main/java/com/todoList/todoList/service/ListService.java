package com.todoList.todoList.service;
import com.todoList.todoList.model.List;
import com.todoList.todoList.rep.ListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * Класс Service - содержит бизнес логику проекта
 */
@Service
public class ListService {

    @Autowired
    private ListRepository todoRep;

    /**
     * Получает список
     * @param id - id списка
     */
    public  List getList(UUID id) {

        Optional<List> list = todoRep.findById(id);
        return list.orElse(null);
    }

    /**
     * Удаление списка задач
     * @param listId - id списка задач
     */
    public void deleteList(UUID listId) {
        todoRep.deleteById(listId);
    }

    /**
     * Создание списка
     * @param title - название списка
     */
    public void createList(String title) {
        List newList = new List();
        newList.setTitle(title);
        newList.setDateCreating(new Date());
        newList.setDateUpdating(new Date());
        todoRep.save(newList);
    }

    /**
     * Изменение списка
     * @param list  - список
     * @param title - название списка
     */
    public void updateList(List list, String title) {
        list.setTitle(title);
        list.setDateUpdating(new Date());
        todoRep.save(list);
    }

    /**
     * Получает все списки задач
     */
    public Iterable<List> getLists() {
        return todoRep.findAll();
    }

    /**
     * Получает направление сортировки
     * @param direction - направление сортировки
     */
    public Sort.Direction getDirection(String direction) {
        if ("desc".equals(direction)) {
            return Sort.Direction.DESC;
        }
        return Sort.Direction.ASC;
    }

    /**
     * Сортировка Фильтрация Пагинация
     * @param dir                   - направление сортировки
     * @param to                    - фильтр по дате (до)
     * @param from                  - фильтр по дате (от)
     * @param numberRecordsOnPage   - кол-во записей в странице
     * @param sortValue             - значение по сортировке
     */
    public ArrayList<List> sortAndFilterAndPag(Sort.Direction dir, String to, String from,
                                    int numberRecordsOnPage, String sortValue) throws ParseException {
        Date toD = new SimpleDateFormat("dd/MM/yyyy kk:mm").parse(to);
        Date fromD = new SimpleDateFormat("dd/MM/yyyy kk:mm").parse(from);

        Pageable sorted = PageRequest.of(0, numberRecordsOnPage, dir, sortValue);

        Iterable<List> listsIt = todoRep.findAllPage(sorted);
        ArrayList<List> lists = new ArrayList<>();

        for (List list : listsIt) {
            if (list.getDateUpdating().after(fromD) && list.getDateUpdating().before(toD)) {
                lists.add(list);
            }
        }
        return lists;
    }
}
