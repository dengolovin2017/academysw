package com.todoList.todoList.controller;

import com.todoList.todoList.exceptions.*;
import com.todoList.todoList.model.List;
import com.todoList.todoList.model.Task;
import com.todoList.todoList.service.TaskService;
import com.todoList.todoList.service.ListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.UUID;


/**
 * TaskController - класс для обработки запросов связанных с задачами (класс Task)
 * Получает, создает, изменяет, удаляет задачи
 */
@RestController
@Api(value = "TaskController")
public class TaskController {

    @Autowired
    ListService service;

    @Autowired
    TaskService taskService;

    /**
     * Получение задачи из списка
     * @param listId - id списка
     * @param taskId - id задачи
     */
    @GetMapping("/lists/{listId}/{taskId}")
    @ApiOperation(value = "get task by id", response = ResponseEntity.class)
    public ResponseEntity<?> getTask(@PathVariable("listId") UUID listId, @PathVariable("taskId") UUID taskId) {
        if (service.getList(listId) != null) {
            if (taskService.getTask(taskId) != null) {
                return new ResponseEntity<>(taskService.getTask(taskId), HttpStatus.OK);
            }
            throw new ExceptionTaskNotFound();
        }
        throw new ExceptionListNotFound();
    }

    /**
     * Создание новой задачи в списоке
     * @param listId    - id списка
     * @param title     - название задачи
     * @param anons     - текст задачи
     * @param urgency   - важность задачи (int)
     */
    @PostMapping("/lists/{listId}")
    @ApiOperation(value = "add task", response = ResponseEntity.class)
    public ResponseEntity<?> addTask(@PathVariable("listId") UUID listId, String title, String anons, Integer urgency) {

        List list = service.getList(listId);
        if (list != null) {
            if (title == null || title.equals("")) {
                throw new ExceptionTitleCannotBeNull();
            } else if (anons == null || anons.equals("")) {
                throw new ExceptionAnonsCannotBeNull();
            } else if (urgency == null) {
                throw new ExceptionUrgencyCannotBeNull();
            }
            taskService.saveTask(list, taskService.createTask(title, anons, urgency));
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            throw new ExceptionListNotFound();
        }
    }

    /**
     * Изменение задачи в списке
     * @param listId    - id списка
     * @param taskId    - id задачи
     * @param title     - название задачи
     * @param anons     - текст задачи
     * @param urgency   - важность задачи
     */
    @PutMapping("/lists/{listId}/{taskId}") //изменение дела в списке
    @ApiOperation(value = "update task", response = ResponseEntity.class)
    public ResponseEntity<?> updateTask(@PathVariable("listId") UUID listId,
                                        @PathVariable("taskId") UUID taskId,
                                        String title, String anons, Integer urgency) {

        List list = service.getList(listId);
        Task task = taskService.getTask(taskId);
        if (list != null) {
            if (task != null) {
                if (title == null || title.equals("")) {
                    throw new ExceptionTitleCannotBeNull();
                } else if (anons == null || anons.equals("")) {
                    throw new ExceptionAnonsCannotBeNull();
                } else if (urgency == null) {
                    throw new ExceptionUrgencyCannotBeNull();
                }
            } else {
                throw new ExceptionTaskNotFound();
            }
            taskService.updateTask(task, title, anons, urgency);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        throw new ExceptionListNotFound();
    }

    /**
     * Удаление задачи из списка
     * @param listId - id списка
     * @param taskId - id задачи
     */
    @DeleteMapping("/lists/{listId}/{taskId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "delete task by id", response = ResponseEntity.class)
    public ResponseEntity<?> deleteTask(@PathVariable("listId") UUID listId, @PathVariable("taskId") UUID taskId) {

        List list = service.getList(listId);
        Task task = taskService.getTask(taskId);
        if (list != null) {
            if (task != null) {
                taskService.deleteTask(taskId);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            throw new ExceptionTaskNotFound();
        } else {
            throw new ExceptionListNotFound();
        }
    }

    /**
     * Помечает задачу выполненной/невыполненной
     * @param listId - id списка
     * @param taskId - id задачи
     */
    @PostMapping("/lists/{listId}/markDone/{taskId}") //пометить список
    @ApiOperation(value = "mark task by id", response = ResponseEntity.class)
    public ResponseEntity<?> markTask(@PathVariable("listId") UUID listId,
                                     @PathVariable("taskId") UUID taskId) {

        List list = service.getList(listId);
        Task task = taskService.getTask(taskId);

        if (list != null) {
            if (task != null) {
                taskService.markTask(task);
                return new ResponseEntity<>(HttpStatus.OK);
            }
            throw new ExceptionTaskNotFound();
        }
        throw new ExceptionListNotFound();
    }
}