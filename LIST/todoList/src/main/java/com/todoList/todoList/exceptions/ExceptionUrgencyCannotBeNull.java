package com.todoList.todoList.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ExceptionUrgencyCannotBeNull - класс исключение
 * Указывает, что важность задачи не может быть пустой
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Urgency cannot be null!")
public class ExceptionUrgencyCannotBeNull extends RuntimeException {
}
