package com.todoList.todoList.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ExceptionTitleCannotBeNull - класс исключение
 * Указывает, что название не может быть пустым
 */
@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Title cannot be null!")
public class ExceptionTitleCannotBeNull extends RuntimeException{

}