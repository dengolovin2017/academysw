package com.todoList.todoList.rep;

import com.todoList.todoList.model.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.UUID;

/**
 * ListRepository - интерфейс репозиторий для списков (List)
 */
@Transactional
@Repository
public interface ListRepository extends CrudRepository<List, UUID> {

    /**
     * Метод, возвращающий записи на странице
     * @param pageable - данные запрошенной страницы
     * @return
     */
    default Page<List> findAllPage(Pageable pageable) {
        return findAllBy(applyDefaultOrder(pageable));
    }

    /**
     * Метод, возвращающий страницу
     * @param pageable
     */
    Page<List> findAllBy(Pageable pageable);


    /**
     * Метод, возвращающий отсортированную страницу
     * @param pageable - данные запрошенной страницы
     * @return
     */
    default Pageable applyDefaultOrder(Pageable pageable) {
        if (pageable.getSort().isUnsorted()) {
            Sort defaultSort = Sort.by("dateUpdating").ascending();
            pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), defaultSort);
        }
        return pageable;
    }

}
