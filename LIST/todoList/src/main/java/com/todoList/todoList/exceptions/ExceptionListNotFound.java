package com.todoList.todoList.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * ExceptionListNotFound - класс исключение
 * Указывает, что список не найден
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "List not found!")
public class ExceptionListNotFound extends RuntimeException{

}
