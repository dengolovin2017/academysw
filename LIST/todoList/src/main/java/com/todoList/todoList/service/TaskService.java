package com.todoList.todoList.service;

import com.todoList.todoList.model.List;
import com.todoList.todoList.model.Task;
import com.todoList.todoList.rep.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRep;

    /**
     * Получает задачу
     * @param id - id задачи
     */
    public Task getTask(UUID id) {

        Optional<Task> task = taskRep.findById(id);
        return task.orElse(null);
    }

    /**
     * Создает новую задачу
     * @param title     - название задачи
     * @param anons     - текст задачи
     * @param urgency   - важность задачи
     */
    public Task createTask(String title, String anons, Integer urgency) {
        Task task = new Task();
        task.setTitle(title);
        task.setAnons(anons);
        task.setUrgency(urgency);
        task.setDateCreating(new Date());
        task.setDateUpdating(new Date());
        return task;
    }

    /**
     * Сохранение задачи
     * @param list - Список
     * @param task - Задача
     */
    public void saveTask(List list, Task task) {
        task.setList(list);
        list.setTasks(task);
        list.setDateUpdating(new Date());
        taskRep.save(task);
    }

    /**
     * Изменение задачи
     * @param task      - задача
     * @param title     - название задачи
     * @param anons     - текст задачи
     * @param urgency   - важность задачи
     */
    public Task updateTask(Task task, String title, String anons, Integer urgency) {
        task.setTitle(title);
        task.setAnons(anons);
        task.setUrgency(urgency);
        task.setDateUpdating(new Date());
        taskRep.save(task);
        return task;
    }

    /**
     * Удаление задачи
     * @param taskId - id задачи
     */
    public void deleteTask(UUID taskId) {
        taskRep.deleteById(taskId);
    }

    /**
     * Помечает задачу выполненной/невыполненной
     * @param task - задача
     */
    public void markTask(Task task) {
        task.setStatus(!task.isStatus());
        taskRep.save(task);
    }
}