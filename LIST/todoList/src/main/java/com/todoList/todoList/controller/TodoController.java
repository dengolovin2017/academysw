package com.todoList.todoList.controller;

import com.todoList.todoList.exceptions.ExceptionListNotFound;
import com.todoList.todoList.exceptions.ExceptionTitleCannotBeNull;
import com.todoList.todoList.model.List;
import com.todoList.todoList.service.ListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.UUID;


/**
 * TodoController - класс для обработки запросов связанных со списками (класс List)
 * Получает, создает, изменяет, удаляет списки
 */
@RestController
@Api(value = "TodoController")
public class TodoController {

    @Autowired
    ListService service;

    /**
     * Получение всех списков
     * @param direction             - направление сортировка (asc, desc)
     * @param sortValue             - значение сортировки (dateCreating, dateUpdating)
     * @param numberRecordsOnPage   - кол-во записей в странице
     * @param from                  - фильтр по дате "от"
     * @param to                    - фильтр по дате "до"
     * @throws ParseException
     */
    @GetMapping(value = "/lists")
    @ApiOperation(value = "get all lists", response = ResponseEntity.class)
    public ResponseEntity<?> getAllLists(String direction, String sortValue, int numberRecordsOnPage,
                                         String from, String to) throws ParseException {

        ArrayList<List> lists = service.sortAndFilterAndPag(service.getDirection(direction),
                to, from, numberRecordsOnPage, sortValue);

        return new ResponseEntity<>(lists, HttpStatus.OK);
    }

    /**
     * Получение списка
     * @param listId - id списка
     */
    @GetMapping("/lists/{listId}")
    @ApiOperation(value = "get list by id", response = ResponseEntity.class)
    public ResponseEntity<?> getList(@PathVariable("listId") UUID listId) {
        if (service.getList(listId) != null) {
            return new ResponseEntity<>(service.getList(listId), HttpStatus.OK);
        } else {
            throw new ExceptionListNotFound();
        }
    }

    /**
     * Удаление списка
     * @param listId - id списка
     */
    @DeleteMapping("/lists/{listId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "delete list by id", response = ResponseEntity.class)
    public ResponseEntity<?> deleteList(@PathVariable("listId") UUID listId) {
        if (service.getList(listId) != null) {
            service.deleteList(listId);
            return new ResponseEntity<>(listId, HttpStatus.OK);
        } else {
            throw new ExceptionListNotFound();
        }
    }

    /**
     * Создание нового списка
     * @param title - название списка
     */
    @PostMapping(value = "/lists")
    @ApiOperation(value = "add list", response = ResponseEntity.class)
    public ResponseEntity<?> addList(String title) {

        if (title == null || title.equals("")) {
            throw new ExceptionTitleCannotBeNull();
        }
        service.createList(title);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Переименование списка
     * @param listId - id списка
     * @param title  - название списка
     */
    @PutMapping(value = "/lists/{listId}")
    @ApiOperation(value = "update list by id", response = ResponseEntity.class)
    public ResponseEntity<?> updateList(@PathVariable("listId") UUID listId, String title) {

        List list = service.getList(listId);
        if (list != null) {
            if (title == null || title.equals("")) {
                throw new ExceptionTitleCannotBeNull();
            }
            service.updateList(list, title);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            throw new ExceptionListNotFound();
        }
    }

    /**
     * Получение всех списков
     */
    @GetMapping(value = "/list")
    @ApiOperation(value = "get all lists without sort and filter", response = ResponseEntity.class)
    public ResponseEntity<?> getLists() {

        Iterable<List> listsIt = service.getLists();
        ArrayList<List> lists = new ArrayList<>();

        for (List list : listsIt) {
            lists.add(list);
        }
        return new ResponseEntity<>(lists, HttpStatus.OK);
    }
}
